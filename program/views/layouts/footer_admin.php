    <div class="page-buffer"></div>
</div>

    <hr class="margin-top">
    <footer id="footer" class="page-footer">
        <div class="footer-container">
            <div class="footer-info">
                <p>ПРИХОДЬТЕ</p>
                <p>вул.Українка,3,Житомир</p>
                <p>11101,Україна</p>

            </div>
            <div class="footer-info">
                <p>КОНТАКТИ</p>
                <p href="#"><i class="fa fa-phone"></i> +38(066)333-22-22</p>
                <p href="#"><i class="fa fa-envelope"></i> apteka@i.ua</p>
            </div>
            <div>
                <p>ПІДПИСУЙТЕСЬ</p>
                <form class="" action="" method="post">
                    <input type="text">
                    <input type="submit" name="Отправить" value="Отправить">
                </form>
            </div>
        </div>
        <div class="comment">
            <?=date("Y");?>.<a href="/">Аптека.Формула здоров'я</a>
        </div>
    </footer>

<script src="/template/js/jquery.js"></script>
<script src="/template/js/jquery.cycle2.min.js"></script>
<script src="/template/js/jquery.cycle2.carousel.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/price-range.js"></script>
<script src="/template/js/jquery.prettyPhoto.js"></script>
<script src="/template/js/main.js"></script>
<script>
    $(document).ready(function(){
        $(".add-to-cart").click(function () {
            var id = $(this).attr("data-id");
            $.post("/cart/addAjax/"+id, {}, function (data) {
                $("#cart-count").html(data);
            });
            return false;
        });
    });
</script>

</body>
</html>